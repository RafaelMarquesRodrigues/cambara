import './css/main.css'
import './css/navbar.css'
import './css/menu.css'
import './css/contact.css'
import './css/section/section-1.css'
import './css/section/section-2.css'
import './css/section/section-3.css'
import './css/section/section-4.css'
import './css/section/section-5.css'
import './css/section/section-6.css'
import './css/section/section-7.css'

import 'isomorphic-unfetch'

function sendContactEmail(){
  const name = document.getElementById('contact-name').value
  const email = document.getElementById('contact-email').value
  const message = document.getElementById('contact-message').value

  //const baseUrl = `${window.location.href}api`
  const baseUrl = '../api/sendContactEmail/index.js'
  console.log(baseUrl)

  fetch(`${baseUrl}?m=${JSON.stringify({ name, email, message })}`)
    .then((res) => {
      console.log(res.text());
    })
    .catch(err => console.log(err));

}

let open = false;

function controllNavbar(e) {
  const navbar = document.getElementById('navbar');
  if (e.target.scrollingElement.scrollTop >= 50) {
    navbar.classList.add('navbar-scrolled');
  } else {
    navbar.classList.remove('navbar-scrolled');
  }
}

function controlMenu(e) {
  const menu = document.getElementById('menu');
  if (!open) {
    menu.classList.add('menu-open');
    open = true;
  } else {
    menu.classList.remove('menu-open');
    open = false;
  }
}

const menu = document.getElementById('menu-btn');
const contact = document.getElementById('contact-btn');

menu.addEventListener('click', controlMenu);
contact.addEventListener('click', sendContactEmail);
window.addEventListener('scroll', controllNavbar);