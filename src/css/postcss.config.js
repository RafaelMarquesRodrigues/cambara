const postcssPresetEnv = require('postcss-preset-env');

module.exports = {
  plugins: [
    postcssPresetEnv({
      stage: 4,
      autoprefixer: {
        grid: true,
        browsers: 'last 4 versions',
      },
    }),
  ],
};
